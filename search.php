<?php
	include 'header.php';
?>

<h1>Search results</h1>

<?php
	if(isset($_POST['submit-search'])){
		$search = mysqli_real_escape_string($conn, $_POST['search']);
		$sql = "SELECT * FROM jobs WHERE title LIKE '%$search%' OR company LIKE '%$search%' OR location LIKE '%$search%' OR description LIKE '%$search%' OR deadline LIKE '%$search%'";
		$result = mysqli_query($conn, $sql);
		$queryResult = mysqli_num_rows($result);
		
		if($queryResult > 0){
			while($row = mysqli_fetch_assoc($result)){
				echo "<div class='role'>
					<h3>".$row['title']."</h3>
					<p> Company: ".$row['company']."</p>
					<p>Location: ".$row['location']."</p>
					<p>Description: ".$row['description']."</p>
					<p class='date'>Deadline: ".$row['deadline']."</p>
				</div>";
			}
		} else{
			echo "No results found";
		}
		
	}
?>