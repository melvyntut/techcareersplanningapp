CREATE TABLE jobs (
	id int(11) not null PRIMARY KEY AUTO_INCREMENT,
	title varchar(256) not null,
	company varchar(256) not null,
	location varchar(256) not null,
	description text not null,
	deadline varchar(256) not null
);

INSERT INTO jobs (title, company, location, description, deadline)
VALUES ('C# Developer', 'Rockstar Leeds', 'Leeds', 'We want a programmer with experience using C#.', '1st February 2020');

INSERT INTO jobs (title, company, location, description, deadline)
VALUES ('Frontend Web Developer', "Sainsbury's", 'Harrogate', 'We want an experienced frontend web developer.', '1st February 2020');

INSERT INTO jobs (title, company, location, description, deadline)
VALUES ('Java Developer', 'HSBC', 'Leeds', 'We want a programmer with experience using Java.', '1st February 2020');

INSERT INTO jobs (title, company, location, description, deadline)
VALUES ('UX Designer', 'Smith Computers', 'Sheffield', 'We want an experienced UX designer.', '23rd February 2020');

INSERT INTO jobs (title, company, location, description, deadline)
VALUES ('Backend Developer', 'Modern Technological Endeavours', 'Pool-in-Wharfedale', 'We want an experienced backend developer.', '17th March 2020');