<?php
	include 'header.php';
?>

	<body>
	<div class="centered">
		<h1>Tech Careers Planning App</h1>
		<p>Search for job roles in Yorkshire. Try entering a keyword...</p>
		<form action="search.php" method="POST">
			<input type="text" name="search" placeholder="Search">
			<button type="submit" name="submit-search">Search</button>
		</form>
	</div>	
	
	</body>
</html>